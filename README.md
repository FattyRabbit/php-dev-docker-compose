phpの開発環境用のdocker-compose
====
[fattyrabbit/php-fpm](https://cloud.docker.com/u/fattyrabbit/repository/docker/fattyrabbit/php-fpm)を利用したローカル開発環境のdocker-compose   
ソース：https://bitbucket.org/FattyRabbit/php-dev-docker-compose/src/master/

## Description
概要：phpの開発（laravelの開発用）のためにlocal環境を作成する目的で、以下を目指しました。
- 出来る限り軽い
- 他のバージョンのPhpを同時に動かす

それで選択したのが「nginx」＋「php-fpm」の構成しました。

## php-fpmのカスタマイズ
元になる[php](https://hub.docker.com/_/php)のfpmは問題点としてListenポートが「9000」固定のところが問題でした。その為にphp-fpm(まずは7.3)のDockerfileを改善してContainerを実行する際に設定出来るように修正を加えました。  
  
ソース：https://bitbucket.org/dockerfile_test/php-fpm/src/master/

## nginx
重要な問題としてテストサーバーの変動があった場合、Dockerfile内でCOPYを利用すると再Buildしても上手く更新されない場合がありましたので、サーバーの管理をvolumesで管理するように設定しました。   
```
    proxy:
        build: nginx
        container_name: proxy
        build:
          context: .
          dockerfile: docker-nginx/Dockerfile
        volumes:
          - ./etc/logs/nginx:/etc/nginx/logs
          - ./etc/nginx/conf.d:/etc/nginx/conf.d
          - ./etc/nginx/letsencrypt:/etc/letsencrypt
          - ./www:/var/www/
        ports:
          - 80:80
          - 443:443
```

## php-fpm
このではListenのポートを「9001」にしてみました。
```
  php-7.3:
    build:
      context: .
      dockerfile: docker-php-7.3/Dockerfile
      args:
        LISTEN_PORT: 9001
    container_name: php-7.3
    volumes:
      - ./www:/var/www/
      - ./etc/php/php.ini:/usr/local/etc/php/php.ini
```

## nginxの設定
実際のウェブサーバーでphpを利用する設定です。
```
   location ~ \.php$ {
        fastcgi_pass    php-7.3:9001;
        fastcgi_index   index.php;
        fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include         fastcgi_params;
    }
```

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[FattyRabbit](https://www.wantedly.com/users/18236110)